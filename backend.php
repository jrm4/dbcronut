<?php

require_once './classes.php';
require_once './dbconn.php';


try {

	$stmt = $conn->prepare("SELECT * FROM subjects");
	$stmt->execute();
	$allfields = $stmt->fetchAll();


    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }




$subject_array = array();

foreach($allfields as $fieldrrow){
    $currsub = new subject();
    
    $currsub->setFirstname($fieldrrow[0]);
    $currsub->setLastname($fieldrrow[1]);
    $currsub->setAge($fieldrrow[2]);
    $currsub->setIncome($fieldrrow[3]);

	/* remember this isn't an overwrite, this ADDS a new subject Object to the end of the already existing array */
   $subject_array[] = $currsub;
    
}

$record_number = 1;

foreach ($subject_array as $newsubject) {
    
    echo "<h1> Record number $record_number </h1>";
    echo "<h2> Subject name:";
    echo $newsubject->getFirstname();
    echo " ";
    echo $newsubject->getLastname();
    echo "</h2> ";
    echo "Age - " . $newsubject->getAge();
    echo "<br>";
    echo " Income - " . $newsubject->getIncome();
    echo "<br>";
    echo " SUBJECT CODE: " . $newsubject->agetocolor() . " " . $newsubject->incometofood();
    echo "<br>";
    
    $record_number++;
              
}
